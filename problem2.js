// 1. Read the given file lipsum.txt
//         2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
//         3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
//         4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
//         5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.

const fs = require('fs');
const path = require('path');

function readFile(filePath) {
    return new Promise((resolve, reject) => {
        fs.readFile(filePath, 'utf8', (err, data) => {
            if (err) {
                reject(err);
            }
            else {
                resolve(data);
            }
        });
    });
}
function writeFile(filePath, data) {
    return new Promise((resolve, reject) => {
        fs.appendFile(filePath, data, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve('File has been saved');
            }
        });
    });
}
function convertIntoUpperCase(data) {
    return new Promise((resolve, reject) => {
        let contentInUpperCase = data.toUpperCase();
        writeFile(path.join(__dirname, 'DataInUpperCase.txt'), contentInUpperCase)
            .then(data => {
                console.log(data);
                resolve('Converted into uppercase');
            }, err => reject(err));
        writeFile(path.join(__dirname, 'filenames.txt'), 'DataInUpperCase.txt ')
            .then(data => console.log(data))
            .catch(err => console.log(err));


    });

}
function convertIntoLowerCase(filePath) {
    return new Promise((resolve, reject) => {
        readFile(filePath).then(data => {

            let lowerCaseSentenceArray = JSON.stringify(data.toLowerCase().split('.'));
            writeFile(path.join(__dirname, 'DataInLowerCase.txt'), lowerCaseSentenceArray)
                .then(data => {
                    console.log(data);
                    resolve('Converted into LowerCase Sentence');
                }, err => reject(err));
            writeFile(path.join(__dirname, 'filenames.txt'), 'DataInLowerCase.txt ')
                .then(data => console.log(data))
                .catch(err => console.log(err));

        });

    });

}

function sortAndStore(filePath) {
    return new Promise((resolve, reject) => {
        readFile(filePath).then(data => {
            let sortTheContent = (JSON.parse(data)).sort((a, b) => {
                return a > b ? 1 : -1;
            });
            writeFile(path.join(__dirname, 'sortedData.txt'), JSON.stringify(sortTheContent))
                .then(data => {
                    console.log(data);
                    resolve('Sorted the Content');
                }, err => reject(err));
            writeFile(path.join(__dirname, 'filenames.txt'), 'sortedData.txt ')
                .then(data => console.log(data))
                .catch(err => console.log(err));
        });
    });
}

function deleteAllFiles(filepath,callback) {
    readFile(filepath).then(data => {
        let fileNames = data.split(' ');
        fileNames.pop();
        //console.log(fileNames);
        fileNames.forEach(fileName => {
            callback(path.join(__dirname,fileName))
            .then(data => console.log(data))
            .catch(err => console.log(err));
        });
    });
}
function deleteFile(filePath) {
    return new Promise((resolve,reject) => {
       fs.unlink(filePath,(err) => {
           if(err) {
               reject(err);
           } else {
               resolve('File is Deleted');
           }
       }); 
    });
}

module.exports ={
    readFile,
    convertIntoUpperCase,
    convertIntoLowerCase,
    sortAndStore,
    deleteFile,
    deleteAllFiles
}
