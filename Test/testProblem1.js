const path = require('path');
const { createDirectory, createFile, deleteFile, deleteAllFiles } = require('../problem1');

createDirectory(path.join(__dirname, 'Data of Json files'))
    .then(data => {
        console.log(data)
        return createFile(path.join(__dirname, 'Data of Json files', 'data1.json'));
    })
    .then(data => {
        console.log(data)
        return createFile(path.join(__dirname, 'Data of Json files', 'data2.json'));
    })
    .then(data => {
        console.log(data)
        deleteAllFiles(path.join(__dirname, 'Data of Json files'), deleteFile);

    }).catch(err => console.log(err));
