const { readFile, convertIntoUpperCase, convertIntoLowerCase, sortAndStore, deleteFile, deleteAllFiles } = require('../problem2')
const path = require('path');

readFile(path.join(__dirname, 'lipsum.txt'))
    .then(data => {
        //console.log(data);
        return convertIntoUpperCase(data);
    })
    .then(data => {
        console.log(data);
        return convertIntoLowerCase(path.join(__dirname, '../DataInUpperCase.txt'));
    })
    .then(data => {
        console.log(data);
        return sortAndStore(path.join(__dirname, '../DataInLowerCase.txt'));
    })
    .then(data => {
        console.log(data);
        deleteAllFiles(path.join(__dirname, '../filenames.txt'), deleteFile);
    });